// gulp styles
const { src, dest, series, watch } = require("gulp");
const concat = require("gulp-concat"); //сцепить
const htmlMin = require("gulp-htmlmin");
const autoprefixer = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");
const svgSprite = require("gulp-svg-sprite");
const image = require("gulp-image");
const uglify = require("gulp-uglify-es").default; //не читаемый код
const babel = require("gulp-babel");
const notify = require("gulp-notify");
const sourcemaps = require("gulp-sourcemaps");
const del = require("del");
const browserSync = require("browser-sync").create();

const cleanDEV = () => {
  return del(["dev"]);
};

const cleanBUILD = () => {
  return del(["dist"]);
};

const resourcesBUILD = () => {
  return src("src/resources/**").pipe(dest("dist"));
};

const resourcesDEV = () => {
  return src("src/resources/**").pipe(dest("dev"));
};

const stylesDEV = () => {
  return src("src/styles/**/*.css")
    .pipe(sourcemaps.init())
    .pipe(concat("main.css"))
    .pipe(autoprefixer({ cascade: false }))
    .pipe(sourcemaps.write())
    .pipe(dest("dev"))
    .pipe(browserSync.stream());
};

const stylesBUILD = () => {
  return src("src/styles/**/*.css")
    .pipe(concat("main.css"))
    .pipe(autoprefixer({ cascade: false }))
    .pipe(cleanCSS({ level: 2 }))
    .pipe(dest("dist"))
    .pipe(browserSync.stream());
};

const htmlDEV = () => {
  return src("src/**/*.html").pipe(dest("dev")).pipe(browserSync.stream());
};

const htmlMinifyBUILD = () => {
  return src("src/**/*.html")
    .pipe(htmlMin({ collapseWhitespace: true }))
    .pipe(dest("dist"))
    .pipe(browserSync.stream());
};

//svg sprite
const svgSprites = () => {
  return src("./src/img/svg/**.svg")
    .pipe(
      svgSprite({
        mode: {
          stack: {
            sprite: "../sprite.svg", //sprite file name
          },
        },
      })
    )
    .pipe(dest("dist/images"));
};
const scriptsDEV = () => {
  return src(["./src/js/components/**.js", "./src/js/main.js"])
    .pipe(sourcemaps.init())
    .pipe(babel({ presets: ["@babel/env"] }))
    .pipe(concat("app.js"))
    .pipe(sourcemaps.write())
    .pipe(dest("dev"))
    .pipe(browserSync.stream());
};

const scriptsBUILD = () => {
  return src(["./src/js/components/**.js", "./src/js/main.js"])
    .pipe(babel({ presets: ["@babel/env"] }))
    .pipe(concat("app.js"))
    .pipe(uglify({ toplevel: true }).on("error", notify.onError()))
    .pipe(dest("dist"))
    .pipe(browserSync.stream());
};

const watchFilesDEV = () => {
  browserSync.init({ server: { baseDir: "dev" } });
};

const watchFilesBUILD = () => {
  browserSync.init({ server: { baseDir: "dist" } });
};

const imagesBUILD = () => {
  return src([
    "./src/images/**.jpg",
    "./src/images/**.png",
    "./src/images/**.jpeg",
    "./src/images/*.svg",
    "./src/images/**/*.jpg",
    "./src/images/**/*.png",
    "./src/images/**/*.jpeg",
  ])
    .pipe(image())
    .pipe(dest("dist/images"));
};

const imagesDEV = () => {
  return src([
    "./src/images/**.jpg",
    "./src/images/**.png",
    "./src/images/**.jpeg",
    "./src/images/*.svg",
    "./src/images/**/*.jpg",
    "./src/images/**/*.png",
    "./src/images/**/*.jpeg",
  ])
    .pipe(image())
    .pipe(dest("dev/images"));
};

watch("src/**/*.html", htmlDEV);
watch("src/**/*.html", htmlMinifyBUILD);
watch("src/styles/*.css", stylesDEV);
watch("src/styles/*.css", stylesBUILD);
watch("src/images/svg/*.svg", svgSprites);
watch("src/js/**/*.js", scriptsDEV);
watch("src/js/**/*.js", scriptsBUILD);
watch("src/resources/**", resourcesDEV);

exports.cleanDEV = cleanDEV;
exports.cleanBUILD = cleanBUILD;
exports.stylesDEV = stylesDEV;
exports.stylesBUILD = stylesBUILD;
exports.scriptsDEV = scriptsDEV;
exports.scriptsBUILD = scriptsBUILD;
exports.htmlDEV = htmlDEV;
exports.htmlMinifyBUILD = htmlMinifyBUILD;

//dev
exports.default = series(
  cleanDEV,
  resourcesDEV,
  htmlDEV,
  scriptsDEV,
  stylesDEV,
  imagesDEV,
  svgSprites,
  watchFilesDEV
);

//dev
exports.dev = series(
  cleanDEV,
  resourcesDEV,
  htmlDEV,
  scriptsDEV,
  stylesDEV,
  imagesDEV,
  svgSprites,
  watchFilesDEV
);

//build
exports.build = series(
  cleanBUILD,
  resourcesBUILD,
  htmlMinifyBUILD,
  scriptsBUILD,
  stylesBUILD,
  imagesBUILD,
  svgSprites,
  watchFilesBUILD
);
