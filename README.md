### Модуль «Веб-вёрстка. Продвинутый уровень»

#### Содержание

1.  Вёрстка HTML-писем

        17 мин. 1.1 Интро
        1.2 Профессия Frontend-разработчик https://www.youtube.com/watch?v=cIqXFpXwU0c
        17 мин. 1.3 Основные правила вёрстки писем
        7 мин. 1.4 Основные почтовики для проверки писем
        26 мин. 1.5 Практика
        21 мин. 1.6 Практика. Часть 2
        1.7 Практическая работа

2.  SVG

        1 мин. 2.1 Интро
        7 мин. 2.2 Как подключать SVG на страницу
        15 мин. 2.3 Создание SVG с нуля
        8 мин. 2.4 Размеры SVG
        4 мин. 2.5 Изменение SVG через css
        6 мин. 2.6 SVG-спрайты
        2.7 Практическая работа
        2.8 CSS анимации https://www.youtube.com/watch?v=W4jkYD3Hazg

3.  Анимация на JS

        1 мин. 3.1 Интро
        15 мин. 3.2 Анимации на чистом JS. SetInterval
        21 мин. 3.3 Анимации на чистом JS. RequestAnimationFrame
        13 мин. 3.4 GreenSock. Базовая анимация
        7 мин. 3.5 GreenSock. Управление анимацией
        1 мин. 3.6 Итоги модуля
        3.7 Практическая работа
        3.8 Веб-анимация средствами JS и CSS

4.  Оптимизация и процесс загрузки

        1 мин. 4.1 Интро
        12 мин. 4.2 Загрузка сайта и метрики
        6 мин. 4.3 Скорость загрузки — css и js
        6 мин. 4.4 Скорость загрузки — шрифты
        11 мин. 4.5 Скорость загрузки — изображения
        4 мин. 4.6 Минификация — изображения
        6 мин. 4.7 Минификация — шрифты
        2 мин. 4.8 Минификация — css
        4 мин. 4.9 Минификация — js
        5 мин. 4.10 Ускорение доставки — CDN
        6 мин. 4.11 Ускорение доставки — кеширование
        5 мин. 4.12 Ускорение доставки — http 2.0
        10 мин. 4.13 Lighthouse
        4.14 Практическая работа

5.  Отрисовка сайта

        1 мин. 5.1 Интро
        1 мин. 5.2 Конвейер пикселей. Парсинг html и css
        2 мин. 5.3 Конвейер пикселей. Расчет стилей
        1 мин. 5.4 Конвейер пикселей. Расчет геометрии
        1 мин. 5.5 Конвейер пикселей. Отрисовка
        3 мин. 5.6 Конвейер пикселей. Компоновка
        5 мин. 5.7 Изменение интерфейса и перерисовка
        9 мин. 5.8 Досрочное вычисление геометрии
        5 мин. 5.9 Приемы для улучшения производительности
        5.10 Практическая работа

6.  Сборщики

        2 мин. 6.1 Интро
        23 мин. 6.2 Gulp. Первые таски
        13 мин. 6.3 Gulp. Watching файлов. Browser-sync
        13 мин. 6.4 Gulp. Обработка картинок, создание SVG-спрайтов
        15 мин. 6.5 Gulp. Обработка javascript
        13 мин. 6.6 Практическая работа. Gulp. Добавление sourcemaps и перенос файлов
        5 мин. 6.7 Parcel
        6.8 Практическая работа

7.  Препроцессоры и Постпроцессоры

        1 мин. 7.1 Интро
        4 мин. 7.2 Идея препроцессинга и постпроцессинга
        13 мин. 7.3 Препроцессор Sass. Синтаксис и конструкции
        17 мин. 7.4 Препроцессор Sass. Практика
        18 мин. 7.5 Препроцессор PUG. Практика
        3 мин. 7.6 PostCSS
        1 мин. 7.7 Итоги

8.  CSS Grid

        11 мин. 8.1 Что такое Grid. Базовые свойства контейнера. Явная сетка
        5 мин. 8.2 Неявная сетка
        3 мин. 8.3 Адаптивный грид
        6 мин. 8.4 Управление элементами сетки
        6 мин. 8.5 Прочие особенности CSS Grid
        12 мин. 8.6 Именованные области сетки
        8.7 Практическая работа
        1 мин. 8.8 Аутро

9.  Будущее верстки

        3 мин. 9.1 Пути развития верстальщика
        9.2 Как найти себя в мире https://www.youtube.com/watch?v=JUgNJNVu1RU&feature=emb_logo
