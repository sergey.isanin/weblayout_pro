#### 5.10 Практическая работа

[](https://go.skillbox.ru/profession/professional-retraining-frontend-developer/dpo-weblayout-advanced/b7d1655d-2faa-4fa4-a5d1-bf09ab6bad1a)

Ваша работа принята

#### Цели задания

Закрепить приемы оптимизации отрисовки сайта на практике.

**Оба задания являются обязательными.**

#### Что нужно сделать

1.  Оптимизируйте анимацию в примере по [ссылке](https://codepen.io/MaxGraph/pen/GRNdQjb).
2.  Оптимизируйте анимацию в примере по [ссылке](https://codepen.io/MaxGraph/details/YzpLepB).

#### Критерии оценки

«Зачёт»

1.  Вы загрузили оптимизированные анимации ссылками на codepen.
2.  Вы выполнили все обязательные задания в соответствии с критериями.

«На доработку»

1.  Вы не загрузили оптимизированные анимации ссылками на codepen.
2.  Прислали задание архивом, а не загрузили задание в гитлаб.

        Папку в гите пустой оставить?
        Все верно - ✅ Да, пустой

```javascript
<div class="block"></div>
```

```css
.block {
  width: 200px;
  height: 200px;
  background-color: navy;
  animation: name-animation 3s infinite;
}

@keyframes name-animation {
  0% {
    transform: scale(1);
  }
  50% {
    transform: scale(2.25);
  }
  100% {
    transform: scale(1);
  }
}
```

```css
.block {
  position: relative;
  width: 300px;
  height: 300px;
  background-color: crimson;
}

.block:hover {
  animation: name-animation 3s infinite;
}

@keyframes name-animation {
  0% {
    left: 0;
    top: 0;
  }
  50% {
    transform: translate(200px, 150px);
  }
  100% {
    left: 0;
    top: 0;
  }
}
```
