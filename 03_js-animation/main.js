var open = document.querySelector(".burger");
var close = document.querySelector(".close");

var tl = gsap.timeline({ paused: true });

tl.fromTo(
  ".menu",
  { display: "none", opacity: 0 },
  { display: "block", duration: 2, opacity: 1, ease: "sine" }
)
  .from(".menu__top", { duration: 0.5, y: -30, opacity: 0, ease: "sine" })
  .from(".nav__list", { duration: 0.5, y: 30, opacity: 0, ease: "sine" })
  .from(".social, .menu__right", {
    duration: 0.5,
    y: 30,
    opacity: 0,
    ease: "sine",
  });
open.onclick = function () {
  tl.play();
};
close.onclick = function () {
  tl.reverse();
};

// ----------------------------------------------------------------

setTimeout(() => {
  gsap.from(".hero__title", {
    opacity: 0,
    y: 100,
    duration: 2,
  });
}, 0);

setTimeout(() => {
  gsap.from(".hero__descr", {
    opacity: 0,
    duration: 6,
  });
}, 0);

setTimeout(() => {
  gsap.from(".hero__btn", {
    opacity: 0,
    y: 100,
    duration: 2,
  });
}, 0);

setTimeout(() => {
  gsap.from(".img-1", {
    opacity: 0,
    duration: 3,
    // scale: 0.8,
  });
}, 0);

setTimeout(() => {
  gsap.from(".img-2", {
    opacity: 0,
    duration: 3,
    scale: 0.8,
  });
}, 0);

setTimeout(() => {
  gsap.from(".img-3", {
    opacity: 0,
    duration: 3,
    scale: 0.8,
  });
}, 0);

setTimeout(() => {
  gsap.from(".photos__author", {
    opacity: 0,
    duration: 10,
  });
}, 0);
