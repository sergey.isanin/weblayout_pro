#### 3.7 Практическая работа

[](https://go.skillbox.ru/profession/professional-retraining-frontend-developer/dpo-weblayout-advanced/97484caa-a06f-4a6e-bf81-10098a01af60)

Ваша работа принята

#### Цель практической работы

Закрепить на практике создание js-анимации посредством библиотеки GreenSock.

#### Что нужно сделать

В прикреплённых к заданию материалах находится вёрстка одноэкранного сайта (без адаптивной версии) и два видео-примера анимации. Примените к этой вёрстке js-анимацию:

1.  С помощью инструментария библиотеки GreenSock реализуйте анимацию загрузки сайта. Учтите, что анимация должна быть максимально похожа на видео.
2.  С помощью инструментария библиотеки GreenSock реализуйте анимацию открытия бургер-меню. Учтите:
    - анимация должна быть максимально похожа на видео;
    - при закрытии меню не нужно прописывать анимацию с нуля — для этого используйте метод reverse().

Если вы хотите сверстать данный блок самостоятельно, используйте [макет в фигме](https://www.figma.com/file/qWcESxGfhz4ZiUrhae8KOO/%D0%94%D0%BB%D1%8F-%D0%B0%D0%BD%D0%B8%D0%BC%D0%B0%D1%86%D0%B8%D0%B8-%D0%B2%D0%B5%D0%B1-%D0%B2%D1%91%D1%80%D1%81%D1%82%D0%BA%D0%B0?node-id=0%3A1).

#### Критерии оценки

**«Зачёт»:**

1.  Оба задания загружены в гитлаб.
2.  Выполнены все обязательные задания в соответствии с критериями.

**«На доработку»:**

1.  Не выполнены все обязательные задания или допущены ошибки в их решении.
2.  Задание прислано архивом, а не загружено в гитлаб.

#### Как отправить задание на проверку

Загрузите домашнее задание в гитлаб.
